Release Notes
-------------
2.0.10
------
* Fixed issue with query array and records having sub models being passed incorrectly into queries.

2.0.9
-----
* Fixed issue with delete.

2.0.8
-----
* Fixed compatibility with new mongo driver.

2.0.7
-----
* Fixed issue with IDs not being updated on the model after being saved.

2.0.6
-----
* Added missing use statement.
* Removed debug code.

2.0.5
-----
* Fixed issue with Mongo ID Object creation.

2.0.4
-----
* Fixed target for delete.

2.0.2
-----
* Removed options from delete.

2.0.1
-----
* Fixed wrong variable reference with delete method.

2.0.0
-----
* Updated the driver to use the latest MongoDB extension and APIs.
 NOTE that this is not 100% complete and only really covers CRUD operations.

1.1.6
-----
* Fixed some issues with MongoId assignments.

1.1.5
-----
* Added DBOptions to upsert.

1.1.4
-----
* Added missing "use" dependency for DBOptions.

1.1.3
-----
* Now using DBOptions type.

1.1.2
-----
* Added missing interface method Mongo::buildColumn.

1.1.1
-----
* Fixed syntax errors.

1.1.0
-----
* New method Mongo::alterColumn with 3 preset modes: add, remove, restore for use when attempting to safely remove a column without destroying data.
* New method Mongo::columnExists for testing if a column has already been created.

1.0.2
-----
* Fixed PDO path.


1.0.1
-----
* Fixed package name.


1.0.0
-----
* Initial Release.

